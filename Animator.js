var Animator = (function (animator) {

    let htmlElement = document.getElementsByTagName("html")[0];
    animator.scrollPage = function (btnArray) {
        for (let i = 0; i < btnArray.length; i++) {
            btnArray[i].addEventListener("click", function () {
                let scrlDest = btnArray[i].getAttribute("href");
                if (scrlDest.includes("#")) {
                    scrlDest = scrlDest.slice(1);
                }
                scrollThere(document.getElementsByClassName(scrlDest)[0]);
            });
        }
    };

    animator.animateSvgThumbnails = function (thumbnails, shape, stroke) {
        for (var i = 0; i < thumbnails.length; i++) {
            let j = i;
            thumbnails[i].addEventListener("mouseover", function () {

                shape[j].classList.add("shape-path-hover");
                stroke[j].classList.add("blue-path-hover");
            });

            thumbnails[i].addEventListener("mouseout", function () {
                shape[j].classList.remove("shape-path-hover")
                stroke[j].classList.remove("blue-path-hover");
            });
        }
    };
    animator.showNavigation = function (navigation, navShowValue) {
        window.addEventListener("scroll", function () {
            if ((htmlElement.scrollTop > navShowValue) || (document.body.scrollTop > navShowValue)) {
                navigation.classList.add("nav-visible");
                navigation.classList.remove("nav-hidden");
            } else {
                navigation.classList.add("nav-hidden");
                navigation.classList.remove("nav-visible");
            }
        });

    };

    function scrollThere(dest) {
        if (!IsIE.detect()) {
            window.scroll({
                behavior: 'smooth',
                left: 0,
                top: dest.offsetTop - 100

            });
        } else {
            window.scrollTo(0, dest.offsetTop - 100);
        }
    }

    return animator;
})(Animator || {});