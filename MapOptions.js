var MapOptions = (function(map){

    map.mapSettings = function() {

        
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(50.7915396, 17.0632586),
        styles: [{
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [{
                "saturation": "-47"
            }, {
                "color": "#272829"
            }, {
                "lightness": "32"
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#000000"
            }, {
                "lightness": 16
            }]
        }, {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#000000"
            }, {
                "lightness": 20
            }]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [{
                "color": "#000000"
            }, {
                "lightness": 17
            }, {
                "weight": 1.2
            }]
        }, {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [{
                "color": "#31485b"
            }, {
                "lightness": "-13"
            }, {
                "saturation": "9"
            }]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [{
                "color": "#000000"
            }, {
                "lightness": 21
            }, {
                "visibility": "off"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "simplified"
            }, {
                "weight": "0.77"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#163146"
            }, {
                "lightness": "-48"
            }, {
                "visibility": "simplified"
            }, {
                "saturation": "-32"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{
                "color": "#163146"
            }, {
                "lightness": "-17"
            }, {
                "weight": 0.2
            }, {
                "saturation": "-25"
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [{
                "color": "#163146"
            }, {
                "lightness": "-9"
            }, {
                "visibility": "on"
            }, {
                "saturation": "10"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [{
                "color": "#000000"
            }, {
                "lightness": 16
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#163146"
            }, {
                "lightness": "-45"
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#163146"
            }, {
                "lightness": "-58"
            }]
        }, {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [{
                "color": "#000000"
            }, {
                "lightness": 19
            }, {
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "color": "#163146"
            }, {
                "lightness": "-30"
            }, {
                "saturation": "11"
            }, {
                "gamma": "1.01"
            }]
        }]
    };
    
    
    var mapElement = document.getElementsByClassName('map')[0];
    
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(50.7915396, 17.0654473),
        map: map,
        title: 'Snazzy!'

        
    });

    };

return map;
})(MapOptions || {});


