var ToggleVisibility = (function (toggler) {
    toggler.switchDivs = function (div1, div2, intervalTime) {
        setInterval(function () {
            setTimeout(function () {
                if (div1.style.display === "block") {
                    toggler.hide(div1);
                    setTimeout(function () {
                        div1.style.display = "none";
                        div2.style.display = "block";
                    }, 1000);
                    setTimeout(function () {
                        div1.style.display = "none";
                        div2.style.display = "block";
                    }, 1000);
                    setTimeout(function () {
                        toggler.show(div2);
                    }, 1200);
                } else if (div1.style.display === "none") {
                    toggler.hide(div2);
                    setTimeout(function () {
                        div2.style.display = "none";
                        div1.style.display = "block";
                    }, 1000);
                    setTimeout(function () {
                        toggler.show(div1);
                    }, 1200);
                }
            }, intervalTime - 1000);
        }, intervalTime);
    };

    toggler.show = function(div) {
        div.getElementsByTagName("h1")[0].classList.remove("hidden");
        div.getElementsByTagName("h1")[0].classList.add("visible");
    };

    toggler.hide = function(div) {
        div.getElementsByTagName("h1")[0].classList.remove("visible");
        div.getElementsByTagName("h1")[0].classList.add("hidden");
    };



    return toggler;
})(ToggleVisibility || {});