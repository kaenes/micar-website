window.onload = function () {
    let nav = document.getElementsByClassName("nav-bar")[0],
        showNavBackValue = 100,
        allButtons = document.getElementsByClassName("scroll-to"),
        thumbnails = document.getElementsByClassName("thumbnail"),
        lightPath = document.getElementsByClassName("light-path"),
        bluePath = document.getElementsByClassName("blue-path"),
        shapePath = document.getElementsByClassName("shape-path"),
        info1 = document.getElementsByClassName("info-1")[0],
        info2 = document.getElementsByClassName("info-2")[0],
        skillSectionTitle = document.getElementsByClassName("sub-header")[0],
        skillItem = document.getElementsByClassName("skill-item"),
        partners = document.getElementsByClassName("partner-element"),
        partnersBackground = document.getElementsByClassName("bg-dark")[0];

  

    for(var i=0; i < partners.length; i++) {
        partners[i].addEventListener("mouseover", function(){
           this.classList.add("moved");
           this.classList.remove("moved-back");
        });

        partners[i].addEventListener("mouseout", function(){
            this.classList.remove("moved");
            this.classList.add("moved-back");
        });
    }

    info1.style.display = "block";
    info2.style.display = "none";

    MapOptions.mapSettings();
    ToggleVisibility.switchDivs(info1, info2, 5000);
    Animator.scrollPage(allButtons);
    Animator.showNavigation(nav, showNavBackValue);
    Animator.animateSvgThumbnails(thumbnails, shapePath, bluePath);


    partnersBackground.addEventListener("mouseover", function(){
        this.classList.add("lightUp");
        this.classList.remove("lightDown");
    });
    partnersBackground.addEventListener("mouseout", function(){
        this.classList.add("lightDown");
        this.classList.remove("lightUp");
    });

    

    this.addEventListener("scroll", function() {
       
       
        for(var i = 0; i < skillItem.length; i++){
           
            if(window.scrollY > 200){
                    skillItem[i].style.transform = "scale(1)";
                    skillItem[i].style.transition = " all 0.5s ease-in";
               
            } else if(window.scrollY < 100) {
                skillItem[i].style.transform = "scale(0)";
                skillItem[i].style.transition = " all 0.5s ease-in";
            }
           
    }

    




        if(window.scrollY > 100){
            skillSectionTitle.style.transform = "scale(1)";
            skillSectionTitle.style.transition = " all 0.5s ease-in";
           
        } else if(window.scrollY < 50){
            skillSectionTitle.style.transform = "scale(0)";
            skillSectionTitle.style.transition = " all 0.5s ease-in";
        }
      
      
    });
   
}