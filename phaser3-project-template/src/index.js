import 'phaser';

var config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

function preload ()
{
    this.load.image('logo', 'assets/logo.png');
    this.load.spritesheet('player', 'assets/playerSpriteSheet.png', {frameWidth: 67, frameHeight: 92});
}

function create ()
{
    var logo = this.add.image(400, 150, 'logo');
    let player = this.add.image(400,300,'player',0);
    console.log(this.game.config.width);
    // player.anchor.set(0.5,0.5);

    this.tweens.add({
        targets: logo,
        y: 450,
        duration: 2000,
        ease: 'Power2',
        yoyo: true,
        loop: -1
    });

}

function update() {

}
